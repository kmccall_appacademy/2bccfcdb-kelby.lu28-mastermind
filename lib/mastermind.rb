class Code

  def self.parse(string)
    new_code = string.upcase.chars.map(&:to_sym)

    if new_code.all? { |pegs| PEGS.include?(pegs) }
      return Code.new(new_code)
    else
      raise "ERROR! Must be 4 letters consisting of 'roygbp'"
    end

  end

  def self.random
    random_string = Array.new(4) { "roygbp".chars.sample }.join
    Code.parse(random_string)
  end

  attr_reader :pegs

  PEGS = {
    R: "red",
    O: "orange",
    Y: "yellow",
    G: "green",
    B: "blue",
    P: "purple"
  }

  def initialize(peg_array)
    @pegs = peg_array
  end

  def [](num)
    @pegs[num]
  end

  def exact_matches(other_code)
    counter = 0

    other_code.pegs.each_with_index do |color, index|
      counter += 1 if color == self.pegs[index]
    end

    return counter
  end

  def near_matches(other_code)
    interset = (@pegs & other_code.pegs).map do |color|
      [color] * [@pegs.count(color), other_code.pegs.count(color)].min
    end.flatten

    interset.size - self.exact_matches(other_code)
  end

  def ==(other_code)
    return false if other_code.class != Code
    self.pegs == other_code.pegs
  end

end

class Game
  attr_reader :secret_code, :guess

  def initialize(code = Code.random)
    @secret_code = code
    @guess
  end

  def get_guess
    puts "Please enter a combination (4) of the available colors."
    input = gets.chomp

    input.length == 4 ? @guess = Code.parse(input) : get_guess
  end

  def display_matches(guess)
    exact = @secret_code.exact_matches(guess)
    near = @secret_code.near_matches(guess)
    puts "Number of pegs exact : #{exact}"
    puts "Number of pegs near : #{near}"
  end

  def play
    puts "Color choices: R, O, Y, G, B, P"
    turn = 1
    turns_allowed = 10
    while turn <= turns_allowed
      get_guess
      display_matches(@guess)
      puts "This is turn #{turn}/10."

      if @secret_code.exact_matches(@guess) == 4
        conclude("win")
        break
      end

      turn += 1
    end

    conclude("loss") if turn > turns_allowed
  end

  private

  def conclude(verdict)
    case verdict
    when "win"
      puts "Congratulations! That code is correct!"
    when "loss"
      puts "Sorry, none of these were correct. Please try again."
    end

    puts "The secret code was #{@secret_code.pegs}."
  end
end

if __FILE__ == $PROGRAM_NAME
  x = Game.new
  x.play
end
